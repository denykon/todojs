var Todo = require('./models/todo');

module.exports = function (app) {

    app.get('/api/todos', function (req, res) {
        Todo.find(function (err, todos) {
            if (err) {
                res.send(err);
            }
            res.json(todos);
        });
    });

    app.post('/api/todos', function (req, res) {
        Todo.create({
            text: req.body.text,
            done: false
        }, function (err, todos) {
            if (err) {
                res.send(err);
            }

            Todo.find(function (err, todos) {
                if (err)
                    res.send(err);
                res.json(todos);
            });
        });

    });

    app.delete('/api/todos/:todo_id', function (req, res) {
        Todo.remove({
            _id: req.params.todo_id
        }, function (err, todos) {
            if (err)
                res.send(err);

            Todo.find(function (err, todos) {
                if (err)
                    res.send(err);
                res.json(todos);
            });
        });
    });

    app.put('/api/todos/:todo_id', function (req, res) {

        var id = req.params.todo_id;
        Todo.findById(id, function (err, found) {
            var status = found.done;
            Todo.update({
                    _id: req.params.todo_id
                },
                {
                    done: !status
                },
                function (err, todos) {
                    if (err)
                        res.send(err);
                });

            Todo.find(function (err, todos) {
                if (err)
                    res.send(err);
                res.json(todos);
            });
        });
    });

    app.get('*', function (req, res) {
        res.sendfile('./public/index.html');
    });
};